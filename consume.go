package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type book struct {
	Author   string `json:"author"`
	Title    string `json:"title"`
	ResultId int    `json:"result_id"`
}

func main() {
	url := "http://localhost:8080"
	res, err := http.Get(url)
	if nil != err {
		panic(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if nil != err {
		panic(err)
	}

	var b book

	if err := json.Unmarshal(body, &b); nil != err {
		panic(err)
	}

	fmt.Printf("Found: (%d) %s by %s\n", b.ResultId, b.Title, b.Author)
}
