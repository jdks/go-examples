package main

import (
  "fmt"
  "encoding/json"
  "log"
)

type Lang struct {
  Name string
  Year int
  URL string
}

func main() {
  lang := Lang{"Go", 2009, "http://golang.org/"}
  data, err := json.MarshalIndent(lang, "", "  ")
  if err != nil {
    log.Fatal(err)
  }
  fmt.Printf("%s\n", data)
}
