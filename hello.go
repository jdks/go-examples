package main

import "fmt"

type Artist struct {
	Name, Genre string
	Songs       int
}

func (a *Artist) newRelease() int {
	a = &Artist{Name: "Jonno", Genre: "RnB", Songs: 20}
	return a.Songs
}

func (a Artist) String() string {
	return a.Name
}

func newRelease(a *Artist) int {
	a.Songs++
	return a.Songs
}

func main() {
	artist := &Artist{Name: "Dave", Genre: "Johnson", Songs: 30}
	fmt.Printf("%s released their %vth song\n", artist, artist.newRelease())
	fmt.Printf("%s has a total of %d songs\n", artist, artist.Songs)
	fmt.Println(&artist)
}
