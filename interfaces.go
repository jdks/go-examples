package main

import "fmt"
import "math"

type Geometry interface {
	area() float64
	perim() float64
}

type Square struct {
	width, height float64
}

type Circle struct {
	radius float64
}

func (s Square) area() float64 {
	return s.width * s.height
}

func (s Square) perim() float64 {
	return 2*s.width + 2*s.height
}

func (c Circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

func (c Circle) perim() float64 {
	return 2 * math.Pi * c.radius
}

func measure(g Geometry) {
	fmt.Println(g)
	fmt.Println(g.area())
	fmt.Println(g.perim())
}

func main() {
	s := Square{3, 4}
	c := Circle{5}

	measure(s)
	measure(c)
}
