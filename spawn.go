package main

import "fmt"
import "os/exec"

func main() {
	dateCmd := exec.Command("date")

	dateOut, err := dateCmd.Output()
	if err != nil {
		panic(err)
	}
	fmt.Println("> date")
	fmt.Println(string(dateOut))

	serverCmd := exec.Command("bundle", "exec", "foreman", "start")
	log, err := serverCmd.Output()
	if err != nil {
		panic(err)
	}
	fmt.Println(string(log))
}
