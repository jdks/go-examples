package main

import (
	"fmt"
	"github.com/codegangsta/cli"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"syscall"
)

func start(appname string) {
	appDir := os.ExpandEnv("${OTB_CODE_DIR}/otb_" + appname + "_app")

	cdErr := os.Chdir(appDir)
	if cdErr != nil {
		panic(cdErr)
	}

	binary, lookErr := exec.LookPath("bundle")
	if lookErr != nil {
		panic(lookErr)
	}

	fd := []uintptr{}

	env := os.Environ()
	procAttr := syscall.ProcAttr{appDir, env, fd, nil}

	args := []string{"bundle", "exec", "foreman", "start"}

	pid, execErr := syscall.ForkExec(binary, args, &procAttr)
	if execErr != nil {
		panic(execErr)
	}

	fmt.Printf("Started process: %d\n", pid)
}

func appsup() map[string][]*os.Process {
	fmt.Println("Running Apps")

	// getParentProcessId
	pidCmd := exec.Command("pidof", "foreman: master")
	pidOutput, _ := pidCmd.Output()
	gids := strings.Split(string(pidOutput), "\n")
	apps := make(map[string][]*os.Process)

	for _, v := range gids {
		groupId := strings.TrimSpace(v)

		// getAppName
		lsofCmd := exec.Command("lsof", "-d", "cwd", "-a", "-p", groupId, "-F", "pn")
		lsofOutput, lsofErr := lsofCmd.Output()
		if lsofErr != nil {
			panic(lsofErr)
		}

		re := regexp.MustCompile("n" + "/home/joshua/code/247/" + "otb_(\\w+)_app")
		appname := string(re.FindSubmatch(lsofOutput)[1])

		// getProcessIds
		psCmd := exec.Command("ps", "-ho", "pid", "--ppid", groupId)
		psOutput, _ := psCmd.Output()
		trimmedOutput := strings.TrimSpace(string(psOutput))

		pids := strings.Split(trimmedOutput, "\n")

		for _, v := range pids {
			output := strings.TrimSpace(v)
			processId, _ := strconv.Atoi(output)
			if processId != 0 {
				process, _ := os.FindProcess(processId)
				apps[appname] = append(apps[appname], process)
			}
		}
	}

	for key, _ := range apps {
		fmt.Println(key)
	}

	return apps
}

func stop(appname string) {
	apps := appsup()
	for _, v := range apps[appname] {
		fmt.Println("killing " + strconv.Itoa(v.Pid))
		v.Kill()
	}
}

func main() {
	app := cli.NewApp()
	app.Name = "otb init"
	app.Usage = "otb init script"
	app.Action = func(c *cli.Context) {
		if len(c.Args()) == 0 {
			panic("not enough args")
		}
		appname := "www"
		if len(c.Args()) == 2 {
			appname = c.Args()[1]
		}
		switch c.Args()[0] {
		case "start":
			start(appname)
		case "stop":
			stop(appname)
		case "appsup":
			appsup()
		}
	}
	app.Run(os.Args)
}
