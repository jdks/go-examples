package main

import "fmt"

func f(left, right chan int) {
	select {
	case v := <-right:
		fmt.Printf("%v <-", left)
		fmt.Println(v + 1)
		left <- 1 + v
	}
}

func main() {
	const n = 4
	leftmost := make(chan int)
	right := leftmost
	left := leftmost
	for i := 0; i < n; i++ {
		right = make(chan int)
		go f(left, right)
		left = right
	}
	go func(c chan int) { c <- 1 }(right)
	fmt.Printf("%+v <-%d\n", leftmost, <-leftmost)
}
