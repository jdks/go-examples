package main

import "fmt"

func main() {
	cities := map[string]string{
		"New York":    "2929292",
		"Los Angeles": "43290393",
		"Chicago":     "48903834",
	}
	for key, value := range cities {
		fmt.Printf("%s has %s inhabitants\n", key, value)
	}
}
