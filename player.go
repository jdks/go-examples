package main

import "fmt"

type User struct {
	Id             int
	Name, Location string
}

func (u *User) Greetings() string {
	return fmt.Sprintf("Hi %s from %s", u.Name, u.Location)
}

type Player struct {
	*User
	GameId int
}

func main() {
	player := &Player{&User{1, "John", "Bristol"}, 20}

	fmt.Println(player.Greetings())
}
