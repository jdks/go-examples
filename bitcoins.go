package main

import "fmt"

var (
	coins = 50
	users = []string{
		"Matthew", "Sarah", "Augustus", "Heidi", "Emilie",
		"Peter", "Giana", "Adriano", "Aaron", "Elizabeth",
	}
	distribution = make(map[string]int, len(users))
)

func main() {
	for _, user := range users {
		var sum int

		for _, letter := range user {
			switch letter {
			case 'a', 'A', 'e', 'E':
				sum++
			case 'i', 'I':
				sum = sum + 2
			case 'o', 'O':
				sum = sum + 3
			case 'u', 'U':
				sum = sum + 4
			}
		}
		if sum > 10 {
			sum = 10
		}
		coins = coins - sum
		distribution[user] = sum
	}

	fmt.Println(distribution)
	fmt.Println("Coins left:", coins)
}
