package main

import (
	"encoding/json"
	"fmt"
)

type Thing struct {
	Name string
	Age  int
}

func main() {
	jsonString := `[
    {"Name":"Dave", "Age":40},
    {"Name":"Joe", "Age":90}
  ]`

	var things []Thing
	err := json.Unmarshal([]byte(jsonString), &things)

	if err != nil {
		// do nothing
	}
	fmt.Printf("%v", things)
}
