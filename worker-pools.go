package main

import "runtime"

func worker(id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		results <- j * 2
	}
}

func main() {
	runtime.GOMAXPROCS(10)

	jobs := make(chan int, 1000000)
	results := make(chan int, 1000000)

	for w := 1; w <= 5; w++ {
		go worker(w, jobs, results)
	}

	for j := 1; j <= 1000000; j++ {
		jobs <- j
	}
	close(jobs)

	for a := 1; a <= 1000000; a++ {
		<-results
	}
}
