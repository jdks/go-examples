package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
)

type Lang struct {
	Name string
	Year int
	URL  string
}

func main() {
	do(func(lang Lang) {
		fmt.Printf("%+v\n", lang)
	})
}

func do(f func(Lang)) {
	input, err := os.Open("/home/joshua/code/go/lang.json")
	if err != nil {
		log.Fatal(err)
	}
	dec := json.NewDecoder(input)
	for {
		var lang Lang
		err := dec.Decode(&lang)
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatal(err)
		}
		f(lang)
	}
}
