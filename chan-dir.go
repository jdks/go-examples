package main

import "fmt"

// accepts a channel for sending values
func ping(pings chan<- string, msg string) {
	pings <- msg
}

// accepts one channel for receiving and another for sending
func pong(pings <-chan string, pongs chan<- string) {
	// receive a message from the input channel
	msg := <-pings

	// send the message to the output channel
	pongs <- msg
}

func main() {
	// make two channels
	pings := make(chan string, 1)
	pongs := make(chan string, 1)

	// pass a message to pings
	ping(pings, "passed message")

	// pass a message from ping to pongs
	pong(pings, pongs)

	// print the message in pongs
	fmt.Println(<-pongs)
}
