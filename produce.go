package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type handler struct{}

type book struct {
	Author   string `json:"author"`
	Title    string `json:"title"`
	ResultId int    `json:"result_id"`
}

func (h *handler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	enc := json.NewEncoder(w)
	resp := &book{"Seth Godin", "The Icarus Deception", 787}

	if err := enc.Encode(resp); nil != err {
		fmt.Fprintf(w, `{"error":"%s"}`, err)
	}
}

func main() {
	http.ListenAndServe(":8080", &handler{})
}
