package main

import "fmt"

var names = []string{
	"Katrina", "Evan", "Neil", "Adam", "Martin", "Matt", "Emma", "Isabella",
	"Emily", "Madison", "Ava", "Olivia", "Sophia", "Abigail", "Elizabeth",
	"Chloe", "Samantha", "Addison", "Natalie", "Mia", "Alexis",
}

func main() {
	var maxLength int

	for _, name := range names {
		if len(name) > maxLength {
			maxLength = len(name)
		}
	}

	store := make([][]string, maxLength)

	for _, name := range names {
		length := len(name)
		store[length-1] = append(store[length-1], name)
	}
	fmt.Printf("%q\n", store)
}
