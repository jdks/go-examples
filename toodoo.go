package main

import (
	"encoding/json"
	"flag"
	"fmt"
)

type Toodoo struct {
	Name     string `json:"name"`
	Complete bool   `json:"is_complete"`
}

func main() {
	flag.Parse()
	arg := flag.Arg(0)

	switch arg {
	case "list":
		list()
	case "add":
		add()
	}
}

func add() {
	fmt.Println("helllooooooo")
}

func list() {
	list := `[
  {
    "name": "Hello world 1",
    "is_complete": false
  },
  {
    "name": "Hello world 2",
    "is_complete": false
  },
  {
    "name": "Hello world 3",
    "is_complete": false
  },
  {
    "name": "Hello world 4",
    "is_complete": true
  },
  {
    "name": "Hello world 5",
    "is_complete": true
  }
  ]`

	var toodoos []Toodoo
	err := json.Unmarshal([]byte(list), &toodoos)

	if err != nil {
		fmt.Println("Errrr mate: ", err)
	}
	fmt.Printf("%v", toodoos)
}
