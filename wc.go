package main

import (
	"strings"

	"code.google.com/p/go-tour/wc"
)

func WordCount(s string) map[string]int {
	fields := strings.Fields(s)
	count := make(map[string]int, len(fields))

	for _, field := range fields {
		count[field]++
	}
	return count
}

func main() {
	wc.Test(WordCount)
}
