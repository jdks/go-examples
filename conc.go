package main

import (
	"fmt"
	"time"
)

func say(s string, r int) {
	for i := 0; i < r; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	go say("world", 5)
	go say("howday", 5)
	say("hello", 1)
}
