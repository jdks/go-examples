package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type cluster struct {
	Name      string `json:"cluster_name"`
	Status    string `json:"status"`
	TimedOut  bool   `json:"timed_out"`
	Nodes     int    `json:"number_of_nodes"`
	DataNodes int    `json:"number_of_data_nodes"`
}

func main() {
	protocol := "http"
	domain := "localhost"
	port := "9200"
	path := "/_cluster/health?level=indices"

	url := protocol + "://" + domain + ":" + port + path

	res, err := http.Get(url)

	if nil != err {
		panic(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if nil != err {
		panic(err)
	}

	var c cluster

	if err := json.Unmarshal(body, &c); nil != err {
		panic(err)
	}

	fmt.Printf("Cluster '%s' is %s\n", c.Name, c.Status)
}
