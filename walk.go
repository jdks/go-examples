package main

import (
	"code.google.com/p/go-tour/tree"
	"fmt"
)

func Walk(tree *tree.Tree, ch chan int) {
	if tree != nil {
		Walk(tree.Left, ch)
		ch <- tree.Value
		Walk(tree.Right, ch)
	}
}

func main() {
	ch := make(chan int)

	go Walk(tree.New(1), ch)

	for value := range ch {
		fmt.Println(value)
	}
}
